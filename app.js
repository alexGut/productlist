window.onload = (event) => {
    _render()
}

function _render() {
    _renderDVD();
    _renderBook();
    _renderFurniture()
}

function _renderDVD() {
    let content = '';

    productList.DVD.forEach(item => {
        content += _getDVDTemplate(item)
    });

    document.getElementById('listOfDVD').innerHTML = content;
}

function _renderBook() {
    let content = '';

    productList.Book.forEach(item => {
        content += _getBookTemplate(item)
    });

    document.getElementById('listOfBook').innerHTML = content;
}

function _renderFurniture() {
    let content = '';

    productList.Furniture.forEach(item => {
        content += _getFurnitureTemplate(item)
    });

    document.getElementById('listOfFurniture').innerHTML = content;
}

function _getDVDTemplate(item){
    let template = '';

    template += `<div class="col">
                    <div class="p-3 border bg-light">
                        <div>
                            <input class="form-check-input delete-checkbox" type="checkbox" value="" aria-label="...">
                        </div>
                        <div class="text-center">
                            <p class="card-text m-0">${item.sku}</p>
                            <p class="card-text m-0">${item.name}</p>
                            <p class="card-text m-0">${item.price}</p>
                            <p class="card-text m-0">${item.size}</p>
                        </div>
                    </div>
                </div>`;

    return template;
}

function _getBookTemplate(item){
    let template = '';

    template += `<div class="col">
                    <div class="p-3 border bg-light">
                        <div>
                            <input class="form-check-input delete-checkbox" type="checkbox" value="" aria-label="...">
                        </div>
                        <div class="text-center">
                            <p class="card-text m-0">${item.sku}</p>
                            <p class="card-text m-0">${item.name}</p>
                            <p class="card-text m-0">${item.price}</p>
                            <p class="card-text m-0">${item.weight}</p>
                        </div>
                    </div>
                </div>`;

    return template;
}

function _getFurnitureTemplate(item){
    let template = '';

    template += `<div class="col">
                    <div class="p-3 border bg-light">
                        <div>
                            <input class="form-check-input delete-checkbox" type="checkbox" value="" aria-label="...">
                        </div>
                        <div class="text-center">
                            <p class="card-text m-0">${item.sku}</p>
                            <p class="card-text m-0">${item.name}</p>
                            <p class="card-text m-0">${item.price}</p>
                            <p class="card-text m-0">${item.height}</p>
                            <p class="card-text m-0">${item.width}</p>
                            <p class="card-text m-0">${item.length}</p>

                        </div>
                    </div>
                </div>`;

    return template;
}