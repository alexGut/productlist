const productList = {};
const storage = new StateStorage();
let data = storage.get();
productList['DVD'] = data ? data.DVD : [];
productList['Book'] = data ? data.Book : [];
productList['Furniture'] = data ? data.Furniture : [];
