
function productTypeChange () {
    let productType = document.getElementById('productType').value;
    let option = document.querySelector('option[value=selectMenu]');
    let dvd = document.getElementById('dvd');
    let furniture = document.getElementById('furniture');
    let book = document.getElementById('book');

    if (option) {
        option.remove();
    }

    dvd.style = book.style = furniture.style = 'display:none';

    if (productType === 'DVD') {
        dvd.style.removeProperty('display');
    }
    if (productType === 'Book') {
        book.style.removeProperty('display');
    }
    if (productType === 'Furniture') {
        furniture.style.removeProperty('display');
    }
}