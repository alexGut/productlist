function saveProduct() {
    let productType = document.getElementById('productType').value;
    let sku = document.getElementById('SKU').value;
    let name = document.getElementById('name').value;
    let price = +document.getElementById('price').value;

    console.log(productList)

    if (productType === 'DVD') {
        productList['DVD'].push({
            'sku': sku,
            'name': name,
            'price': price,
            'size': document.getElementById('size').value
        })
    }
    if (productType === 'Book') {
        productList['Book'].push({
            'sku': sku,
            'name': name,
            'price': price,
            'weight': document.getElementById('bookWeight').value
        })
    }
    if (productType === 'Furniture') {
        productList['Furniture'].push({
            'sku': sku,
            'name': name,
            'price': price,
            'height': document.getElementById('height').value,
            'width': document.getElementById('width').value,
            'length': document.getElementById('length').value
        })
    }
    storage.set(productList);
}
