class StateStorage {
    constructor(key) {
        this.key = key || "scandiweb";
    }

    set(data) {
        if (data) {
            localStorage.setItem(this.key, JSON.stringify(data))
        } else {
            console.log('You try to save empty state')
        }
    }

    get() {
        let result = localStorage.getItem(this.key);
        if (result) {
            return JSON.parse(result)
        } else return null;
    }
}

